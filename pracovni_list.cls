\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{pracovni_list}[2018/10/02 Pracovni listy pro matematicky krouzek]

\LoadClass[12pt]{article}
\RequirePackage[a4paper,margin=2.5cm]{geometry}
\RequirePackage{times}
\RequirePackage[czech]{babel}
\RequirePackage[utf8]{inputenc}
\RequirePackage{tcolorbox}
\tcbuselibrary{skins}
\RequirePackage{marginnote}
\RequirePackage[square,numbers]{natbib}

\RequirePackage{hyperref}
\RequirePackage{import}


\bibliographystyle{elsarticle-harv}
\renewcommand{\bibsection}{}
\renewcommand{\bibfont}{\scriptsize}
\renewcommand{\bibsep}{2pt}
\newcommand{\save}[1]{#1}

\definecolor{mycolor}{rgb}{0.122, 0.435, 0.698}

\renewcommand{\maketitle}[1]{%
\noindent{\bfseries{\Large \color{mycolor} Zábavná matematika | Pracovní list #1}}%
\vskip 8mm%
\noindent\emph{Jméno a příjmení}\, \hrulefill%
}

\newcounter{myProblemNumber}

\newcommand{\priklad}[4]{%
%\clearpage%
\stepcounter{myProblemNumber}%
\smallskip%
\save{%
\marginnote{%
	\begin{tcolorbox}[enhanced,hbox,colframe=mycolor,colback=mycolor,boxsep=0pt,arc=0pt,tikz={rotate=90,transform shape}]
		\color{white} #1
	\end{tcolorbox}
}[-7mm]}
\noindent{\bfseries{\themyProblemNumber}.}\quad #4%
\vspace*{-2mm}
\save{\noindent\zdroj{#2}{#3}~\hrulefill\par}
}

\newcommand{\klokan}{Klokan}
\newcommand{\prijimacky}{Přijímačky}
\newcommand{\hlavolamy}{Hlavolamy}
\newcommand{\hry}{Hry}
\newcommand{\info}{Informatika}

\newcommand{\zdroj}[2]{\par\noindent{\small \emph{Zdroj}:~\cite[#1]{#2}}}
\newcommand{\varianty}[4]{%
	\begin{tabular}{llll}
		(A)~#1 & (B)~#2 & (C)~#3 & (D)~#4	
	\end{tabular}
}

\newcommand{\obrazekavariantysvg}[7]{
\begin{flushleft}%
	\begin{minipage}{#6\textwidth}%
		\varianty{#2}{#3}{#4}{#5}%
	\end{minipage}
	%
	\begin{minipage}{#7\textwidth}%
		\def\svgwidth{\textwidth}%
		\import{figures/}{#1.pdf_tex}
	\end{minipage}
\end{flushleft}
}

\newcommand{\obrazekavariantypdf}[7]{
	\begin{flushleft}%
		\begin{minipage}{#6\textwidth}%
			\varianty{#2}{#3}{#4}{#5}%
			\end{minipage}
			%
			\begin{minipage}{#7\textwidth}%
				\includegraphics[width=\textwidth]{figures/#1.pdf}
				\end{minipage}
				\end{flushleft}
}

\endinput