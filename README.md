# matematicky-krouzek

Pracovní listy připravené pro výuku v [Univerzitní základní škole Lvíčata](http://lvicata.cvut.cz) při [ČVUT v Praze](https://www.cvut.cz), volně šiřitelné pro nekomerční účely. V případě jakýchkoliv dotazů mě prosím kontaktujte na [jan.zeman@cvut.cz](mailto:jan.zeman@cvut.cz).

